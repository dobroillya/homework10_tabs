const listBtns = document.querySelector(".tabs");
const text = document.querySelectorAll("[data-text]");

//create menu
const textArr = [...text];
const menuList = textArr.reduce((result, item) => {
  result.push(item.dataset.text);
  return result;
}, []);

function createmenu() {
  const menu = menuList.map(
    (item) => `<li class="tabs-title" data-open="${item}">${item}</li>`
  );
  console.log(menu.join(""));
  listBtns.innerHTML = menu.join("");
}
createmenu();

const itemList = document.querySelectorAll(".tabs-title");

//active button state
function toggleActive() {
  itemList.forEach((i) =>
    i.classList.contains("active") ? i.classList.remove("active") : 0
  );
}

//display chosen text
function showText(targetText) {
  text.forEach((i) => {
    i.classList.add("hidden");
    if (i.getAttribute("data-text") === targetText) {
      i.classList.remove("hidden");
    }
  });
}

//default active state
itemList[0].classList.add("active");
showText(itemList[0].getAttribute("data-open"));

//listen buttons
listBtns.addEventListener("click", (e) => {
  const targetTextName = e.target.getAttribute("data-open");
  toggleActive();
  e.target.classList.toggle("active");
  showText(targetTextName);
});
